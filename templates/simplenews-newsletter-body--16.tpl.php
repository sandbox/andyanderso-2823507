<?php
   /**
    * @file
    * Default theme implementation to format the simplenews newsletter body.
    *
    * Copy this file in your theme directory to create a custom themed body.
    * Rename it to override it. Available templates:
    *   simplenews-newsletter-body--[tid].tpl.php
    *   simplenews-newsletter-body--[view mode].tpl.php
    *   simplenews-newsletter-body--[tid]--[view mode].tpl.php
    * See README.txt for more details.
    *
    * Available variables:
    * - $build: Array as expected by render()
    * - $build['#node']: The $node object
    * - $title: Node title
    * - $language: Language code
    * - $view_mode: Active view mode
    * - $simplenews_theme: Contains the path to the configured mail theme.
    * - $simplenews_subscriber: The subscriber for which the newsletter is built.
    *   Note that depending on the used caching strategy, the generated body might
    *   be used for multiple subscribers. If you created personalized newsletters
    *   and can't use tokens for that, make sure to disable caching or write a
    *   custom caching strategy implemention.
    *
    * @see template_preprocess_simplenews_newsletter_body()
    */
   ?>
<?php
   global $base_path, $base_url;
   $url = $base_url;
   $author = user_load($build['#node']->uid);
   $forecaster_name = theme_get_setting('forecaster_name_field','responsive_sac');
   $display_name = field_get_items('user', $author, $forecaster_name);
   $company = field_get_items('user', $author, 'field__company_link');
   $duration=$build['field_duration']['#items'][0]['value'];
   $site_name = variable_get('site_name');
   $nws_name = theme_get_setting('local_nws_name','responsive_sac');
   $nws_url = theme_get_setting('local_nws_url','responsive_sac');
   $wx_low = theme_get_setting('wx_elevation_low','responsive_sac');
   $wx_high = theme_get_setting('wx_elevation_high','responsive_sac');
   $current_wx_desc = theme_get_setting('current_wx_conditions_desc','responsive_sac');
   $show_regions = theme_get_setting('show_forecast_region','responsive_sac');
   $wx_map_page = theme_get_setting('wx_map_page','responsive_sac');
   $wx_table_page = theme_get_setting('wx_table_page','responsive_sac');
   $obs_page = theme_get_setting('obs_page','responsive_sac');
   $submit_snowpack_obs_page = theme_get_setting('submit_snowpack_obs_page','responsive_sac');
   $submit_avalanche_obs_page = theme_get_setting('submit_avalanche_obs_page','responsive_sac');
   $upper_band = theme_get_setting('upper_elevation_band','responsive_sac');
   $mid_band = theme_get_setting('middle_elevation_band','responsive_sac');
   $lower_band = theme_get_setting('lower_elevation_band','responsive_sac');
   $email_header_img = file_create_url(file_load(theme_get_setting('email_header_img','responsive_sac'))->uri);
   $logo = theme_get_setting('logo','responsive_sac');
   $show_danger_rose = theme_get_setting('enable_danger_rose_view','responsive_sac');

   //for Flathead and basic only sites
   $labels = array('No Rating', '1. Low', '2. Moderate', '3. Considerable', '4. High', '6. Extreme');

   $upperdr = $build['field_danger_rating_3']['#items'][0]['value'];
   $upper_dr = $labels[$upperdr];

   $middr = $build['field_danger_rating_2']['#items'][0]['value'];
   $mid_dr = $labels[$middr];

   $lowerdr = $build['field_danger_rating_1']['#items'][0]['value'];
   $low_dr = $labels[$lowerdr];


   if (isset($build['field_type_1']['#items'][0]['value'])) {
       $key1 = $build['field_type_1']['#items'][0]['value'];
       $av_types_info1 = field_info_field('field_type_1');
       $av_types1 = $av_types_info1['settings']['allowed_values'];
       $type_1 = $av_types1[$key1];
   }

   if (isset($build['field_type_2']['#items'][0]['value'])) {
       $key2 = $build['field_type_2']['#items'][0]['value'];
       $av_types_info2 = field_info_field('field_type_2');
       $av_types2 = $av_types_info2['settings']['allowed_values'];
       $type_2 = $av_types2[$key2];
   }

   if (isset($build['field_type_3']['#items'][0]['value'])) {
       $key3 = $build['field_type_3']['#items'][0]['value'];
       $av_types_info3 = field_info_field('field_type_3');
       $av_types3 = $av_types_info3['settings']['allowed_values'];
       $type_3 = $av_types3[$key3];
   }

   ?>
<?php
   //Enable below to show all Array Variables of Form

   /*print '<pre>';
   print render($build['field_danger_rating_3']);
   //print_r($build['field_danger_rating_3']['und'][0]['value']);
   print_r($build['field_danger_rating_1']);
   print '</pre>';
   */
   ?>
<div>
   <div style="margin-left:auto;margin-right:auto;max-width:1060px;font-size: 1.071em;">
     <div style="background: #333;">
       <a target="_blank" href="<?php print $url; ?>/" title="Home" rel="home"><img src="<?php print $logo; ?>"/>
       </a>
       <strong style="float: right;">
         <a target="_blank" <a style="font-weight:bold;color:#ffffff;text-decoration:none;font-size: 28px;text-transform: uppercase;margin:50px;" href="<?php print $url; ?>/" title="Home" rel="home"><?php echo $site_name; ?>
         </a>
       </strong>
       <!--DONATE ask and button. Delete or comment out ask text after Dec. 31 -->
       <div style="padding: 20px 10px;background: #333;">
         <span style="color: #fff;padding: 0px 10px 0px 10px;">Funding for the daily avalanche forecast relies on the generous support of users like you. Please DONATE today!</span>
         <a href="https://www.sierraavalanchecenter.org/donate/forecast-email" style="margin: -10px 0px 0px 0px;font-size: 16px;font-weight: bold;text-transform: uppercase;text-align: center;float: right;background: none repeat scroll 0 0 #CE4141;padding: 10px 10px;color: #ffffff;">Donate
         </a>
       </div>
     </div>
      <div>
         <!--BEGIN PUBLISHED DATE AND FORECASTER NAME-->
         <div style="background-color:#444;color:#eee;display:inline-block;padding:5px;width:99%;line-height:1.5em">
            <div align="left" hspace="5" vspace="5" style="font-size:17px;text-transform:uppercase;padding-left:5px;font-weight:bold;float:left">
               <?php
                  $forecastdate = date("F j, Y \@ G:i", $build['#node']->created);
                  echo 'Avalanche Forecaster published on '.$forecastdate;
                  ?>
            </div>
            <div align="right" hspace="5" vspace="5" style="text-align:right;padding-right:5px;font-size:14px;float:right">
               <?php if ($duration <= '120'){
                  print "<span style='font-weight:bold; text-transform:uppercase'>This forecast is valid for $duration hours</span><br>";
                  }
                  ?>Issued by <?php print render($display_name[0]['name_line']);?> - <?php print render($company[0]['title']);?>
            </div>
         </div>
         <!--END PUBLISHED DATE AND FORECASTER NAME-->
         <?php if ($show_danger_rose  != 0): ?>
         <!--BEGIN BOTTOM LINE WITH DANGER ROSE-->
         <?php if (isset($build['field_bottom_line']['#items'][0]['value'])): ?>
         <div style="padding:10px 10px 20px 0;border:1px solid #ddd">
            <div>
               <div align="left" hspace="5" vspace="5" style="float:left;width:220px">
                  <img style="margin:0px;padding:0px" src="<?php print $url.$build['field_overall_danger_rose']['#items'][0]['img_detailed_rose']; ?>"/>
               </div>
               <div style="padding-left:180px">
                  <span style="font-size:17px;line-height:2em;text-transform:uppercase">bottom line</span>
                  <?php print $build['field_bottom_line']['0']['#markup']; ?>
                  <p style="text-align:right;text-transform:uppercase">
                     <a target="_blank" href="<?php print $url; ?>/how-to-read-avalanche-advisory">How to read the forecast
                     </a>
                  </p>
               </div>
            </div>
            <?php if ($build['field_overalldanger']['#items'][0]['value']>0):?>
            <div>
               <ul id="danger-rating-bar-advanced" style="list-style:none;width:100%;max-width:745px;background: #fff !important;text-align: center;">
                  <li style="border: 1px solid #aaaaaa;font-size: 13px;background: #fff;display: inline-block;padding: 0px 2px 0px 5px;height: 70px;vertical-align: top;max-width: 545px;">
                     <a href="http://www.avalanche.org/danger_card.php" target="_blank">
                     <img style="height:50px !important;" id="danger-rating-bar-advanced-img" src="/sites/all/themes/responsive_sac/img/rating-icons/<?php print $build['field_overalldanger']['#items'][0]['value'];?>.png">
                     </a>
                  </li>
                  <li style="border: 1px solid #aaaaaa;font-size: 13px;background: #fff;display: inline-block;padding: 0px 2px 0px 5px;height: 70px;vertical-align: top;max-width:270px;">
                     <div style="display: flex;justify-content: center;flex-direction: column;height: 70px;">
                        <?php
                           $dr = $build['field_overalldanger']['#items'][0]['value'];
                           $ta = array('','Generally safe avalanche conditions. Watch for
                           unstable snow on isolated terrain features.','Heightened avalanche conditions on specific terrain features. Evaluate snow and terrain carefully; identify features of concern.','Dangerous avalanche conditions. Careful snowpack evaluation, cautious route-finding and conservative
                           decision-making essential.', 'Very dangerous avalanche conditions. Travel in avalanche terrain not recommended.', 'Avoid all avalanche terrain.');
                           $ta_text = $ta[$dr];
                           print $ta_text; ?>
                     </div>
                  </li>
               </ul>
            </div>
            <?php endif; ?>
         </div>
         <?php endif; ?>
         <!--END BOTTOM LINE WITH DANGER ROSE-->
         <?php endif; ?>
         <?php if ($show_danger_rose  == 0): ?>
         <!--BEGIN BOTTOM LINE AND MTN IMG-->
         <?php if (isset($build['field_bottom_line']['#items'][0]['value'])): ?>
         <div id="avalanche-danger-row" class="darkbg simple" style="background-color:#444;padding:5px">
            <table>
               <tr>
                  <td style="width:37%">
                     <h3 style="color:#fff;margin: 0px;">
                        <?php foreach($build['field_forecast_region']['#items'] as $tag) {
                           $region = $tag['taxonomy_term']->name."<br>";
                           print $region;
                           }
                           ?>
                     </h3>
                  </td>
                  <td style="text-align:right;width:20%">
                     <a id="how-to-read-advisory-simple" style="color:#018fe2" href="<?php print $url;?>/how-to-read-avalanche-advisory">HOW TO READ THE FORECAST</a>
                  </td>
               </tr>
            </table>
         </div>
         <div style="border: 5px solid #444;">
            <div style="margin: -13px -10px 0px;position: relative;background-color: #444;">
               <h3 style="font-weight: bold;color: #eee;text-transform: uppercase;font-size: 17px;padding: 7px;margin:0px;">Bottom Line</h3>
            </div>
            <div style="padding: 20px 10px 10px 10px;">
               <?php if (isset($build['field_bottom_line']['#items'][0]['value'])) { print $build['field_bottom_line']['#items'][0]['value']; } ?>
            </div>
         </div>
         <table style="background-color:#444; color:#eeeeee;width:100%;text-align:center;">
            <tr style="padding:0px;margin:0px">
               <td style="padding-bottom:5px;margin:0px;height:60px">
                  <ul style="padding-bottom:5px;margin:0px;border-bottom:1px solid #eeeeee;">
                     <li style="display:inline-block;"><img class="upper-dli dli" src="<?php print $url.'/sites/all/themes/responsive_sac/img/h60-email-upper-mountain.png';?>"/></li>
                     <li style="display:inline-block;padding:0px;margin:0px">
                        <div>
                           <h2 style="padding:0px;margin:0px"><?php print $upper_dr; ?></h2>
                           <p style="padding:0px;margin:0px"><?php print $upper_band; ?></p>
                        </div>
                     </li>
                     <li style="display:inline-block;"><img class="upper-dli dli" src="<?php print $url.'/sites/all/themes/responsive_sac/img/dli/'.$upperdr.'.png'; ?>" />
                     </li>
                  </ul>
               </td>
            <tr style="padding:0px;margin:0px">
               <td style="padding-bottom:5px;margin:0px;height:60px">
                  <ul style="padding-bottom:5px;margin:0px;border-bottom:1px solid #eeeeee;">
                     <li style="display:inline-block;"><img class="upper-dli dli" src="<?php print $url.'/sites/all/themes/responsive_sac/img/h60-email-mid-mountain.png';?>"/></li>
                     <li style="display:inline-block;padding:0px;margin:0px">
                        <div>
                           <h2 style="padding:0px;margin:0px"><?php print $mid_dr; ?></h2>
                           <p style="padding:0px;margin:0px"><?php print $mid_band; ?></p>
                        </div>
                     </li>
                     <li style="display:inline-block;"><img class="upper-dli dli" src="<?php print $url.'/sites/all/themes/responsive_sac/img/dli/'.$middr.'.png'; ?>" />
                     </li>
                  </ul>
               </td>
            </tr>
            <tr style="padding:0px;margin:0px">
               <td style="padding:0px;margin:0px;">
                  <ul style="padding-bottom:5px;margin:0px;border-bottom:1px solid #eeeeee;">
                     <li style="display:inline-block;"><img class="upper-dli dli" src="<?php print $url.'/sites/all/themes/responsive_sac/img/h60-email-lower-mountain.png';?>"/></li>
                     <li style="display:inline-block;padding:0px;margin:0px">
                        <div>
                           <h2 style="padding:0px;margin:0px"><?php print $low_dr; ?></h2>
                           <p style="padding:0px;margin:0px"><?php print $lower_band; ?></p>
                        </div>
                     </li>
                     <li style="display:inline-block;"><img class="upper-dli dli" src="<?php print $url.'/sites/all/themes/responsive_sac/img/dli/'.$lowerdr.'.png'; ?>" />
                     </li>
                  </ul>
               </td>
            </tr>
         </table>
         <?php if ($build['field_overalldanger']['#items'][0]['value']>0):?>
         <div>
            <ul id="danger-rating-bar-advanced" style="margin-top: 0px;margin-bottom: 0px;list-style:none;background-color:#444; color:#eeeeee;text-align: center;padding:20px;">
               <li style="font-size: 13px;display: inline-block;margin:0px;">
                  <div style="display: flex;justify-content: center;flex-direction: column;">
                     <?php
                        $dr = $build['field_overalldanger']['#items'][0]['value'];
                        $ta = array('','Generally safe avalanche conditions. Watch for
                        unstable snow on isolated terrain features.','Heightened avalanche conditions on specific terrain features. Evaluate snow and terrain carefully; identify features of concern.','Dangerous avalanche conditions. Careful snowpack evaluation, cautious route-finding and conservative
                        decision-making essential.', 'Very dangerous avalanche conditions. Travel in avalanche terrain not recommended.', 'Avoid all avalanche terrain.');
                        $ta_text = $ta[$dr];
                        print $ta_text; ?>
                  </div>
               </li>
            </ul>
         </div>
         <?php endif; ?>
         <div id="danger-scale-bar-container" style="display: flex;width: 100%;background: #454545;padding-bottom: 15px;margin-bottom: 10px;">
           <ul id="danger-scale-bar" style="list-style-type: none;margin: auto;padding: 0;overflow: hidden;">
             <a href="http://www.avalanche.org/danger_card.php" target="_blank">
               <li id="low-li-item" class="danger-scale-rating-item" style="float:left;padding:4px 20px;text-transform:uppercase;border-right:1px solid #000;font-weight:bold;font-size:10px;color:#000;background-color:#00cc00;">1. Low</li>
               <li id="mod-li-item" class="danger-scale-rating-item" style="float:left;padding:4px 20px;text-transform:uppercase;border-right:1px solid #000;font-weight:bold;font-size:10px;color:#000;background-color:#ffff33;">2. Moderate</li>
               <li id="cons-li-item" class="danger-scale-rating-item" style="float:left;padding:4px 20px;text-transform:uppercase;border-right:1px solid #000;font-weight:bold;font-size:10px;color:#000;background-color:#ffaa33;">3. Considerable</li>
               <li id="high-li-item" class="danger-scale-rating-item" style="float:left;padding:4px 20px;text-transform:uppercase;border-right:1px solid #000;font-weight:bold;font-size:10px;color:#000;background-color:#cc0000;">4. High</li>
               <li id="extr-li-item" class="danger-scale-rating-item" style="float:left;padding:4px 20px;text-transform:uppercase;border-right:1px solid #000;font-weight:bold;font-size:10px;color:#eee;background-color:#000;">5. Extreme</li>
             </a>
           </ul>
         </div>
      </div>
      <?php endif; ?>
      <!--END BOTTOM LINE AND MTN IMG-->
      <?php endif; ?>
      <!--AVALANCHE PROBLEMS -->
      <!-- Prob 1 -->
      <?php if (isset($build['field_type_1']['#items'][0]['value'])): ?>
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Avalanche Problem #1: <?php print $type_1;?></span>
         <ul style="text-align:center;list-style:none;font-size:17px;padding-left:0px">
            <li style="display:inline-block;width:200px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Type
               </div>
               <div style="padding-top:10px;min-height:185px;border-bottom:1px solid #ddd;border-right:1px solid #ddd">
                  <a target="_blank" href="<?php print $url;?>/avalanche-problems#<?php print $build['field_type_1']['#items'][0]['value'];?>"><img height="175px" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/api/<?php print $build['field_type_1']['#items'][0]['value'];?>.png" /></a>
               </div>
            </li>
            <li style="display:inline-block;width:200px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Aspect/Elevation
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img style="margin:0px;padding:0px" height="175px" src="<?php print $url.$build['field_rose_1']['#items'][0]['img_detailed_rose'];?>"/>
               </div>
            </li>
            <?php if (isset($build['field_likelihood_1']['#items'][0]['value'])): ?>
            <li style="display:inline-block;width:180px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Likelihood
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img height="175px" src="<?php print $url;?>/sites/all/themes/responsive_sac/img/slider/likelihood-<?php print $build['field_likelihood_1']['#items'][0]['value'];?>.png" />
               </div>
            </li>
            <?php endif; ?>
            <?php if (isset($build['field_size_1']['#items'][0]['value'])): ?>
            <li style="display:inline-block;width:180px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Size
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img height="175px" src="<?php print $url;?>/sites/all/themes/responsive_sac/img/slider/size-<?php print $build['field_size_1']['#items'][0]['value']; print $build['field_size_1']['#items'][1]['value'];?>.png" />
               </div>
            </li>
            <?php endif; ?>
            <!-- TREND not used
               <?php if (isset($build['field_trend_1']['#items'][0]['value'])): ?>
                      <li style="display:inline-block;width:180px;vertical-align:top">
                        <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                          Trend
                        </div>
                        <div style="border-bottom:1px solid #ddd;border-right:none!important;min-height:185px">
                          <div><img style="padding-top:70px" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/trend/<?php print $build['field_trend_1']['#items'][0]['value'];?>.png" /></div>
                   <div style="color:#333;text-transform:uppercase;font-size:14px;font-weight:bold;text-align:center">
                            <?php $a = array('','Decreasing Danger','Same Danger','Increasing Danger');
                  $trend_1=$build['field_trend_1']['#items'][0]['value'];
                  $trend_text_1 = $a[$trend_1];
                  print $trend_text_1;
                   ?>
               </div>
                        </div>
                      </li>
               <?php endif; ?>
               -->
         </ul>
         <div style="line-height:1.5em;padding:10px"><?php print $build['field_description_1']['0']['#markup']; ?></div>
      </div>
      <?php endif; ?>
      <!--END Prob 1 -->
      <!-- Prob 2 -->
      <?php if (isset($build['field_type_2']['#items'][0]['value'])): ?>
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Avalanche Problem #2: <?php print $type_2;?></span>
         <ul style="text-align:center;list-style:none;font-size:17px;padding-left:0px">
            <li style="display:inline-block;width:200px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Type
               </div>
               <div style="padding-top:10px;min-height:185px;border-bottom:1px solid #ddd;border-right:1px solid #ddd">
                  <a target="_blank" href="<?php print $url;?>/avalanche-problems#<?php print $build['field_type_2']['#items'][0]['value'];?>"><img  height="175px" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/api/<?php print $build['field_type_2']['#items'][0]['value'];?>.png" /></a>
               </div>
            </li>
            <li style="display:inline-block;width:200px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Aspect/Elevation
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img style="margin:0px;padding:0px" height="175px" src="<?php print $url.$build['field_rose_2']['#items'][0]['img_detailed_rose'];?>"/>
               </div>
            </li>
            <?php if (isset($build['field_likelihood_2']['#items'][0]['value'])): ?>
            <li style="display:inline-block;width:180px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Likelihood
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img height="175px" src="<?php print $url;?>/sites/all/themes/responsive_sac/img/slider/likelihood-<?php print $build['field_likelihood_2']['#items'][0]['value'];?>.png" />
               </div>
            </li>
            <?php endif; ?>
            <?php if (isset($build['field_size_2']['#items'][0]['value'])): ?>
            <li style="display:inline-block;width:180px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Size
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img height="175px" src="<?php print $url;?>/sites/all/themes/responsive_sac/img/slider/size-<?php print $build['field_size_2']['#items'][0]['value']; print $build['field_size_2']['#items'][1]['value'];?>.png" />
               </div>
            </li>
            <?php endif; ?>
            <!-- TREND not used
               <?php if (isset($build['field_trend_2']['#items'][0]['value'])): ?>
                      <li style="display:inline-block;width:180px;vertical-align:top">
                        <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                          Trend
                        </div>
                        <div style="border-bottom:1px solid #ddd;border-right:none!important;min-height:185px">
                          <div><img style="padding-top:70px" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/trend/<?php print $build['field_trend_2']['#items'][0]['value'];?>.png" /></div>
                   <div style="color:#333;text-transform:uppercase;font-size:14px;font-weight:bold;text-align:center">
                            <?php $a = array('','Decreasing Danger','Same Danger','Increasing Danger');
                  $trend_2=$build['field_trend_2']['#items'][0]['value'];
                  $trend_text_2 = $a[$trend_2];
                  print $trend_text_2;
                   ?>
               </div>
                        </div>
                      </li>
               <?php endif; ?>
                -->
         </ul>
         <div style="line-height:1.5em;padding:10px"><?php print $build['field_description_2']['0']['#markup']; ?></div>
      </div>
      <?php endif; ?>
      <!--END Prob 2 -->
      <!-- Prob 3 -->
      <?php if (isset($build['field_type_3']['#items'][0]['value'])): ?>
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Avalanche Problem #3: <?php print $type_3;?></span>
         <ul style="text-align:center;list-style:none;font-size:17px;padding-left:0px">
            <li style="display:inline-block;width:200px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Type
               </div>
               <div style="padding-top:10px;min-height:185px;border-bottom:1px solid #ddd;border-right:1px solid #ddd">
                  <a target="_blank" href="<?php print $url;?>/avalanche-problems#<?php print $build['field_type_3']['#items'][0]['value'];?>"><img height="175px" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/api/<?php print $build['field_type_3']['#items'][0]['value'];?>.png" /></a>
               </div>
            </li>
            <li style="display:inline-block;width:200px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Aspect/Elevation
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img style="margin:0px;padding:0px" height="175px" src="<?php print $url.$build['field_rose_3']['#items'][0]['img_detailed_rose'];?>"/>
               </div>
            </li>
            <?php if (isset($build['field_likelihood_3']['#items'][0]['value'])): ?>
            <li style="display:inline-block;width:180px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Likelihood
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img height="175px" src="<?php print $url;?>/sites/all/themes/responsive_sac/img/slider/likelihood-<?php print $build['field_likelihood_3']['#items'][0]['value'];?>.png" />
               </div>
            </li>
            <?php endif; ?>
            <?php if (isset($build['field_size_3']['#items'][0]['value'])): ?>
            <li style="display:inline-block;width:180px;vertical-align:top">
               <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                  Size
               </div>
               <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:195px">
                  <img height="175px" src="<?php print $url;?>/sites/all/themes/responsive_sac/img/slider/size-<?php print $build['field_size_3']['#items'][0]['value']; print $build['field_size_3']['#items'][1]['value'];?>.png" />
               </div>
            </li>
            <?php endif; ?>
            <!-- TREND not used
               <?php if (isset($build['field_trend_3']['#items'][0]['value'])): ?>
                      <li style="display:inline-block;width:180px;vertical-align:top">
                        <div style="border-bottom:1px solid #ddd;text-transform:uppercase">
                          Trend
                        </div>
                        <div style="border-bottom:1px solid #ddd;border-right:none!important;min-height:185px">
                          <div><img style="padding-top:70px" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/trend/<?php print $build['field_trend_3']['#items'][0]['value'];?>.png" /></div>
                   <div style="color:#333;text-transform:uppercase;font-size:14px;font-weight:bold;text-align:center">
                            <?php $a = array('','Decreasing Danger','Same Danger','Increasing Danger');
                  $trend_3=$build['field_trend_3']['#items'][0]['value'];
                  $trend_text_3 = $a[$trend_3];
                  print $trend_text_3;
                   ?>
               </div>
                        </div>
                      </li>
               <?php endif; ?>
               -->
         </ul>
         <div style="line-height:1.5em;padding:10px"><?php print $build['field_description_3']['0']['#markup']; ?></div>
      </div>
      <?php endif; ?>
      <!-- END Prob 3 -->
      <!--END AVALANCHE PROBLEMS -->
      <!--BEGIN AVALANCHE TEXT DISCUSSION-->
      <?php if ($build['field_text_discussion']['#items'][0]['value'] != ''): ?>
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">FORECAST discussion</span>
         <div style="margin-bottom:5px;padding:5px;clear:both">
            <div style="padding:10px">
               <?php print $build['field_text_discussion']['0']['#markup']; ?>
            </div>
         </div>
      </div>
      <?php endif; ?>
      <!--END AVALANCHE TEXT DISCUSSION-->
      <!--BEGIN RECENT OBS-->
      <?php if ($build['field_recent_activity']['#items'][0]['value'] != ''): ?>
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444"><?php if (!empty($obs_page)):?><a style="color:#eee;" href="<?php print $url.$obs_page; ?>"><?php endif;?>recent observations<?php if (!empty($obs_page)):?></a><?php endif;?></span>
         <div style="margin-bottom:5px;padding:5px;clear:both;min-height:165px;height:auto;">
            <div align="left" hspace="5" vspace="5" style="float:left;width:150px;padding-top:5px;">
               <?php if (!empty($submit_snowpack_obs_page)):?>
               <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                  <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$submit_snowpack_obs_page; ?>">Submit Observations</a>
               </div>
               <?php endif;?>
               <?php if (!empty($submit_avalanche_obs_page)):?>
               <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                  <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$submit_avalanche_obs_page; ?>">Submit Avalanche Observations</a>
               </div>
               <?php endif;?>
            </div>
            <div style="padding-left:150px">
               <?php print $build['field_recent_activity']['0']['#markup']; ?>
            </div>
         </div>
      </div>
      <?php endif; ?>
      <!--END RECENT OBS-->
      <!--BEGIN WX Section-->
      <?php if ($build['field_mountain_weather']['#items'][0]['value'] != '' || isset($build['field_temp8700']['#items']['0']['value']) || isset($build['field_today7to8weather']['#items']['0']['value'])): ?>
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Weather and CURRENT CONDITIONS</span>
         <!-- BEGIN MOUNTAIN WEATHER-->
         <?php if ($build['field_mountain_weather']['#items'][0]['value'] != ''): ?>
         <div style="padding-top: 10px;">
            <span style="padding: 5px;color: #333;font-style: italic;text-transform: uppercase;border-bottom: 1px solid #ddd;font-size: 13px;">weather</span>
            <div style="padding: 10px 5px 5px;clear: both;min-height: 100px;">
               <div style="padding:10px">
                  <?php print $build['field_mountain_weather']['0']['#markup']; ?>
               </div>
            </div>
         </div>
         <?php endif; ?>
         <!-- END MOUNTAIN WEATHER-->
         <!--BEGIN WEATHER OBS - CURRENT CONDITIONS-->
         <?php if (isset($build['field_temp8700']['#items']['0']['value'])): ?>
         <div>
            <span style="padding: 5px;color: #333;font-style: italic;text-transform: uppercase;border-bottom: 1px solid #ddd;font-size: 13px;">CURRENT CONDITIONS</span><span style="color: #444;margin-bottom: 5px;">&nbsp; <?php print $current_wx_desc;?></span>
            <div style="padding: 10px 5px 5px;clear: both;min-height: 100px;">
               <div align="left" hspace="5" vspace="5" style="float:left;width:150px;padding-top:5px;">
                  <?php if (!empty($wx_map_page)):?>
                  <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                     <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$wx_map_page; ?>">Weather Station Map</a>
                  </div>
                  <?php endif;?>
                  <?php if (!empty($wx_table_page)):?>
                  <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                     <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$wx_table_page; ?>">Weather Station Table</a>
                  </div>
                  <?php endif;?>
               </div>
               <table style="padding:5px;margin:0px;width:80%;border:none">
                  <tbody>
                     <tr>
                        <td style= "background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           6am temperature:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_temp8700']['#items']['0']['value']);?> deg. F.
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Max. temperature:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24maxtemp']['#items']['0']['value']);?> deg. F.
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Average ridgetop wind direction:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24winddir']['#items']['0']['value']);?>
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Average ridgetop wind speed:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24windspeed']['#items']['0']['value']);?> mph
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Maximum ridgetop wind gust:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24maxgust']['#items']['0']['value']);?> mph
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           New snowfall:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24snowfall']['#items']['0']['value']);?> inches
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Total snow depth:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_totalsnowdepth']['#items']['0']['value']);?> inches
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <?php endif; ?>
         <!--TWO DAY WEATHER FORECAST-->
         <?php if (isset($build['field_today7to8weather']['#items']['0']['value'])): ?>
         <div>
            <span class="wx-sub-title" style="padding: 5px;color: #333;font-style: italic;text-transform: uppercase;border-bottom: 1px solid #ddd;font-size: 13px;">Two-Day Mountain Weather Forecast Produced in partnership with the <a href="<?php print $nws_url;?>" target="_blank"><?php print $nws_name;?></a>
             </span>
            <!-- Weather Table Responsive -->
            <article style="width:100%;max-width:1000px;margin:0 auto;height: auto;position:relative;">
               <div class="wxfxelevations" style="width: 100%;text-align: center;padding: 20px 25px 5px 12%;font-weight: normal;color: #333;font-style: italic;text-transform: uppercase;font-size: 13px;">
                  <span class="txt-l"><?php print $wx_low;?></span>
              </div>
               <table id="wxforecast" class="row-sub-table" style="border-collapse:separate;border-spacing:5px 0px;padding:5px;margin:0px;table-layout:fixed;width:100%;">
                  <tr class="wxtoprow">
                     <th class="hide wxfirstcol" style="height:53px;text-align:center;display:table-cell !important;border:0;background:none;"></th>
                     <th class="default" style="height:53px;text-align:center;display:table-cell !important;border-bottom:1px solid #ddd;border-top-left-radius:15px;border-top-right-radius:15px;background:#ddd !important;">
                        <?php
                           if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                             print "Today";
                           }
                           else {
                             echo date("l", $build['#node']->created);
                           }
                           ?>
                     </th>
                     <th class="bg-light" style="height:53px;text-align:center;display:table-cell !important;border-top:2px solid #ddd;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top-left-radius:15px;border-top-right-radius:15px;">
                        <?php
                           if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                             print "Tonight";
                           }
                           else {
                             echo date("l", $build['#node']->created)." Night";
                           }
                           ?>
                     </th>
                     <th style="height:53px;text-align:center;display:table-cell !important;border-bottom:1px solid #ddd;border-top-left-radius:15px;border-top-right-radius:15px;background:#ddd  !important;">
                        <?php
                           $tomorrow  = strtotime('+1 day', $build['#node']->created);
                           echo date("l", $tomorrow);
                           ?>
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Weather:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-l"><?php print render($build['field_today7to8weather']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-l"><?php print render($build['field_tonight7to8weather']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-l"><?php print render($build['field_tomorrow7to8weather']['#items']['0']['value']) ?></span></td>
                     </tr>
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Temperatures:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today7to8temp']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight7to8temp']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow7to8temp']['#items']['0']['value']) ?></span></td>
                     </tr>
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Mid Slope Winds:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today7to8winddirection']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight7to8winddirection']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow7to8winddirection']['#items']['0']['value']) ?></span></td>
                     </tr>
                     <!--
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Wind Speed:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today7to8windspeed']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight7to8windspeed']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow7to8windspeed']['#items']['0']['value']) ?></span></td>
                     </tr>
                     -->
                     <tr class="wxbottomrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Expected snowfall:</td>
                        <td class="default" style="border-top: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;border-bottom-left-radius:15px;border-bottom-right-radius:15px;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today7to8snow']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-bottom:2px solid #ddd;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom-left-radius:15px;border-bottom-right-radius:15px;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight7to8snow']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;border-bottom-left-radius:15px;border-bottom-right-radius:15px;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow7to8snow']['#items']['0']['value']) ?></span></td>
                     </tr>
                  </tbody>
               </table>
               <div class="wxfxelevations" style="width: 100%;text-align: center;padding: 20px 25px 5px 12%;font-weight: normal;color: #333;font-style: italic;text-transform: uppercase;font-size: 13px;">
                  <span class="txt-l"><?php print $wx_high;?></span>
                </div>
               <table id="wxforecast" class="row-sub-table" style="border-collapse:separate;border-spacing:5px 0px;padding:5px;margin:0px;table-layout:fixed;width:100%;">
                  <tr class="wxtoprow">
                     <th class="hide wxfirstcol" style="height:53px;text-align:left;display:table-cell !important;border:0;background:none;"></th>
                     <th class="default" style="height:53px;text-align:center;display:table-cell !important;border-bottom:1px solid #ddd;border-top-left-radius:15px;border-top-right-radius:15px;background:#ddd !important;">
                        <?php
                           if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                             print "Today";
                           }
                           else {
                             echo date("l", $build['#node']->created);
                           }
                           ?>
                     </th>
                     <th class="bg-light" style="height:53px;text-align:center;display:table-cell !important;border-top:2px solid #ddd;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top-left-radius:15px;border-top-right-radius:15px;">
                        <?php
                           if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                             print "Tonight";
                           }
                           else {
                             echo date("l", $build['#node']->created)." Night";
                           }
                           ?>
                     </th>
                     <th style="height:53px;text-align:center;display:table-cell !important;border-bottom:1px solid #ddd;border-top-left-radius:15px;border-top-right-radius:15px;background:#ddd  !important;">
                        <?php
                           $tomorrow  = strtotime('+1 day', $build['#node']->created);
                           echo date("l", $tomorrow);
                           ?>
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Weather:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-l"><?php print render($build['field_today8to9weather']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-l"><?php print render($build['field_tonight8to9weather']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-l"><?php print render($build['field_tomorrow8to9weather']['#items']['0']['value']) ?></span></td>
                     </tr>
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Temperatures:</td>
                        <td class="default" style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today8to9temp']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight8to9temp']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow8to9temp']['#items']['0']['value']) ?></span></td>
                     </tr>
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Ridge Top Winds:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today8to9winddirection']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight8to9winddirection']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow8to9winddirection']['#items']['0']['value']) ?></span></td>
                     </tr>
                     <!--
                     <tr class="wxmidrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Wind Speed:</td>
                        <td class="default" style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today8to9windspeed']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom:1px solid #ddd;border-top:1px solid #ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight8to9windspeed']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;border-bottom: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow8to9windspeed']['#items']['0']['value']) ?></span></td>
                     </tr>
                   -->
                     <tr class="wxbottomrow">
                        <td class="wxfirstcol" style="border:none !important;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;height:53px;text-align:center;display:table-cell !important;">Expected snowfall:</td>
                        <td class="default" style="border-top: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;border-bottom-left-radius:15px;border-bottom-right-radius:15px;height:53px;text-align:center;display:table-cell;"><span class="txt-1"><?php print render($build['field_today8to9snow']['#items']['0']['value']) ?></span></td>
                        <td style="background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;border-bottom:2px solid #ddd;border-right:2px solid #ddd;border-left:2px solid #ddd;border-bottom-left-radius:15px;border-bottom-right-radius:15px;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tonight8to9snow']['#items']['0']['value']) ?></span></td>
                        <td style="border-top: 1px solid #fff;background-color:#fff;vertical-align:middle;line-height:1.5em;font-size:13px;background:#ddd;border-bottom-left-radius:15px;border-bottom-right-radius:15px;height:53px;text-align:center;display:table-cell !important;"><span class="txt-1"><?php print render($build['field_tomorrow8to9snow']['#items']['0']['value']) ?></span></td>
                     </tr>
                  </tbody>
               </table>
            </article>
         </div>
      </div>
      <?php endif; ?>
      <?php endif; ?>
      <!--END TWO DAY WEATHER FORECAST-->
      <!--BEGIN DISCLAIMER-->
      <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
         <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Disclaimer</span>
         <div style="margin-bottom:5px;padding:5px;clear:both">
            <div style="padding:10px">
               <?php print $build['field_disclaimer']['#items'][0]['value']; ?>
            </div>
         </div>
      </div>
   </div>
   <!-- end disclaimer-->
</div>
</div>
