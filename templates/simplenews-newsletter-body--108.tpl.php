<?php
   /**
    * @file
    * Default theme implementation to format the simplenews newsletter body.
    *
    * Copy this file in your theme directory to create a custom themed body.
    * Rename it to override it. Available templates:
    *   simplenews-newsletter-body--[tid].tpl.php
    *   simplenews-newsletter-body--[view mode].tpl.php
    *   simplenews-newsletter-body--[tid]--[view mode].tpl.php
    * See README.txt for more details.
    *
    * Available variables:
    * - $build: Array as expected by render()
    * - $build['#node']: The $node object
    * - $title: Node title
    * - $language: Language code
    * - $view_mode: Active view mode
    * - $simplenews_theme: Contains the path to the configured mail theme.
    * - $simplenews_subscriber: The subscriber for which the newsletter is built.
    *   Note that depending on the used caching strategy, the generated body might
    *   be used for multiple subscribers. If you created personalized newsletters
    *   and can't use tokens for that, make sure to disable caching or write a
    *   custom caching strategy implemention.
    *
    * @see template_preprocess_simplenews_newsletter_body()
    */
   ?>
<style type="text/css">@media screen and (max-width: 720px) {
   div.snowpack-summary #problem-type {
   float: none !important;
   }
   div#problem-canned-text { 
   margin: 0px 20px 0px 0px!important;
   padding: 0px 10px;
   min-width: 190px;
   }
   div.problem-text {
   height:270px !important;
   }
   div.snowpack-summary .problem-char-wrapper {
   height: auto !important;
   }
   }
</style>
<?php 
   global $base_path, $base_url;
         $url = $base_url;
         $node = $variables['node'];
         $author = user_load($build['#node']->uid);
         $forecaster_name = theme_get_setting('forecaster_name_field','responsive_sac');
         $display_name = field_get_items('user', $author, $forecaster_name);
         $company = field_get_items('user', $author, 'field__company_link');
         $userid = $node->uid;
         $duration=$node->field_duration['und'][0]['value'];
         $expduration = $duration*3600;
         $forecastdate = date("F j, Y \@ g:i a", $node->created);
         $forcastTimestamp = strtotime(date("F j, Y G:i", $node->created));
         $expdateTimestamp = $node->created + $expduration;
         $expdate = date("F j, Y \@ g:i a", $expdateTimestamp);
         $now = date("F j, Y G:i");
         $nowTimestamp = strtotime($now);
         $now1 = new DateTime();
         $now1->setTimestamp($now);
         $future_date = new DateTime();
         $future_date->setTimestamp($expdateTimestamp);
         $timeLeftToExpire = $future_date->diff($now1);
         $nws_name = theme_get_setting('local_nws_name','responsive_sac');
         $nws_url = theme_get_setting('local_nws_url','responsive_sac');
         $wx_low = theme_get_setting('wx_elevation_low','responsive_sac');
         $wx_high = theme_get_setting('wx_elevation_high','responsive_sac');
         $current_wx_desc = theme_get_setting('current_wx_conditions_desc','responsive_sac');
         $show_regions = theme_get_setting('show_forecast_region','responsive_sac');
         $wx_map_page = theme_get_setting('wx_map_page','responsive_sac');
         $wx_table_page = theme_get_setting('wx_table_page','responsive_sac');
         $obs_page = theme_get_setting('obs_page','responsive_sac');
         $submit_snowpack_obs_page = theme_get_setting('submit_snowpack_obs_page','responsive_sac');
         $submit_avalanche_obs_page = theme_get_setting('submit_avalanche_obs_page','responsive_sac');
         $upper_band = theme_get_setting('upper_elevation_band','responsive_sac');
         $mid_band = theme_get_setting('middle_elevation_band','responsive_sac');
         $lower_band = theme_get_setting('lower_elevation_band','responsive_sac');
         $email_header_img = file_create_url(file_load(theme_get_setting('email_header_img','responsive_sac'))->uri);
         $logo = theme_get_setting('logo','responsive_sac');
         $site_name = variable_get('site_name');
   
   if (isset($build['field_type_1']['#items'][0]['value'])) {
       $key1 = $build['field_type_1']['#items'][0]['value'];
       $av_types_info1 = field_info_field('field_type_1');
       $av_types1 = $av_types_info1['settings']['allowed_values'];
       $type_1 = $av_types1[$key1];
   }
   
   if (isset($build['field_type_2']['#items'][0]['value'])) {
       $key2 = $build['field_type_2']['#items'][0]['value'];
       $av_types_info2 = field_info_field('field_type_2');
       $av_types2 = $av_types_info2['settings']['allowed_values'];
       $type_2 = $av_types2[$key2];
   }
   
   if (isset($build['field_type_3']['#items'][0]['value'])) {
       $key3 = $build['field_type_3']['#items'][0]['value'];
       $av_types_info3 = field_info_field('field_type_3');
       $av_types3 = $av_types_info3['settings']['allowed_values'];
       $type_3 = $av_types3[$key3];
   }
   
   ?>
<div style="font-family: 'PT Sans';font-size: 1.071em;">
   <div style="margin-left:auto;margin-right:auto;max-width:1060px">
      <div style="background-color:#ffffff">
         <div style="background:#333333;float:none;margin:0px!important;padding:15px">
            <div style="font-size:28px;text-transform:uppercase;word-spacing:5px 15px">
               <strong><a target="_blank" style="font-family:PT Sans;font-weight:bold;color:#ffffff;text-decoration:none" href="<?php print $url; ?>/" title="Home" rel="home"><span><?php echo $site_name; ?></span></a></strong>
            </div>
         </div>
         <div style="background-image:url('<?php print $email_header_img; ?>');background-repeat:no-repeat">
            <a target="_blank" href="<?php print $url; ?>/" title="Home" rel="home"><img src="<?php print $logo; ?>"/></a>
         </div>
      </div>
      <div>
         <!--BEGIN PUBLISHED DATE AND FORECASTER NAME-->
         <div style="background-color:#444;color:#eee;display:inline-block;padding:5px;width:99%;line-height:1.5em">
            <div align="left" hspace="5" vspace="5" style="font-size:17px;text-transform:uppercase;padding-left:5px;font-weight:bold;float:left">
               <?php 
                  $forecastdate = date("F j, Y \@ G:i", $build['#node']->created);
                  foreach($build['field_forecast_region']['#items'] as $tag) {
                  $region = $tag['taxonomy_term']->name;
                  }
                  echo $region.' Snowpack Summary<br>published on '.$forecastdate.':';
                  ?>
            </div>
            <div align="right" hspace="5" vspace="5" style="text-align:right;padding-right:5px;font-size:14px;float:right">
               <?php if ($duration <= '120'){
                  print "<span style='font-weight:bold; text-transform:uppercase'>This snowpack summary is valid for $duration hours</span><br>";
                  }
                  ?>Issued by <?php print render($display_name[0]['name_line']);?> - <?php print render($company[0]['title']);?>
            </div>
         </div>
         <!--END PUBLISHED DATE AND FORECASTER NAME-->
         <!--BEGIN BOTTOM LINE-->
         <?php if (isset($build['field_bottom_line']['#items'][0]['value'])):?> 
         <div style="border:1px solid #ddd;margin-bottom:5px;padding-top:5px;clear:both">
            <div>
               <span style="color: #eee;padding: 7px;margin: 0px;text-transform: uppercase;font-weight: bold;font-size: 17px;background-color: #444;">Bottom Line</span>
               <br />
               <div id="bottom-line-adv" class="snowpack-summary-div-text" style="padding: 20px;font-size: 1.071em;">
                  <?php if (isset($build['field_bottom_line']['#items'][0]['value'])) { print render($build['field_bottom_line']['#items'][0]['value']); } ?>
               </div>
            </div>
         </div>
         <?php endif; ?> 
         <!--AVALANCHE PROBLEMS -->
         <!-- Prob 1 -->
         <?php if (isset($build['field_type_1']['#items'][0]['value'])): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Avalanche Problem #1: <?php print $type_1;?></span>
            <ul style="text-align:center;list-style:none;padding-left:0px;width:100%;max-width:7150px">
               <li style="display:inline-block;width:200px;vertical-align:top">
                  <div style="border-bottom:1px solid #ddd;text-transform:uppercase;font-size:17px;">
                  </div>
                  <div style="padding-top:10px;min-height:185px;border-bottom:1px solid #ddd;border-right:1px solid #ddd">
                     <a target="_blank" href="<?php print $url;?>/avalanche-problems#<?php print $build['field_type_1']['#items'][0]['value'];?>"><img style="height:185px;" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/api/<?php print $build['field_type_1']['#items'][0]['value'];?>.png" /></a>
                  </div>
               </li>
               <li style="display:inline-block;width:100%;max-width:470px;vertical-align:top">
                  <div style="border-bottom:1px solid #ddd;text-transform:uppercase;font-size:17px;">
                  </div>
                  <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:200px;font-size: 1.071em;">
                     <div class="problem-text" style="display:flex;justify-content:center;flex-direction:column;height: 175px;padding: 0px 10px;">
                        <?php $ac = $build['field_type_1']['#items'][0]['value']; 
                           $ct = array('','Storm Slab avalanches release naturally during snow storms and can be triggered for a few days after a storm. They often release at or below the trigger point. They exist throughout the terrain. Avoid them by waiting for the storm snow to stabilize.','Deep Slab avalanches are destructive and deadly events that can release months after the weak layer was buried. They are scarce compared to Storm or Wind Slab avalanches. Their cycles include fewer avalanches and occur over a larger region. You can triggered them from well down in the avalanche path, and after dozens of tracks have crossed the slope. Avoid the terrain identified in the forecast and give yourself a wide safety buffer to address the uncertainty.','Wind Slab avalanches release naturally during wind events and can be triggered for up to a week after a wind event. They form in lee and cross-loaded terrain features. Avoid them by sticking to wind sheltered or wind scoured areas.','Wet Slab avalanches occur when there is liquid water in the snowpack, and can release during the first few days of a warming period. Travel early in the day and avoid avalanche terrain when you see pinwheels, roller balls, loose wet avalanches, or during rain-on-snow events.','Persistent Slab avalanches can be triggered days to weeks after the last storm. They often propagate across and beyond terrain features that would otherwise confine Wind and Storm Slab avalanches. In some cases they can be triggered remotely, from low-angle terrain or adjacent slopes. Give yourself a wide safety buffer to address the uncertainty.','Loose Wet avalanches occur when water is running through the snowpack, and release at or below the trigger point. Avoid very steep slopes and terrain traps such as cliffs, gullies, or tree wells. Exit avalanche terrain when you see pinwheels, roller balls, a slushy surface, or during rain-on-snow events.','Loose Dry avalanches exist throughout the terrain, release at or below the trigger point, and can run in densely-treed areas. Avoid very steep slopes and terrain traps such as cliffs, gullies, or tree wells.','Generally safe avalanche conditions. Watch for unstable snow on isolated terrain features. Use normal caution when travelling in the backcountry.','Cornice Fall avalanches are caused by a release of overhanging, wind drifted snow. Cornices form on lee and cross-loaded ridges, sub-ridges, and sharp convexities. They are easiest to trigger during periods of rapid growth from wind drifting, rapid warming, or during rain-on-snow events. Cornices may break farther back onto flatter areas than expected.','Glide avalanches occur when water lubricates the interface between the snowpack and the ground. The entire snowpack releases. These avalanches are difficult to predict. Avoid the terrain identified in the forecast or below glide cracks.'
                           );
                           $ct_text = $ct[$ac];
                           print $ct_text; ?>
                     </div>
                  </div>
               </li>
            </ul>
            <div style="line-height:1.5em;padding:10px;font-size: 1.071em;"><?php print $build['field_description_1']['#items'][0]['value']; ?></div>
         </div>
         <?php endif; ?>
         <!--End problem 1 -->
         <!-- Prob 2 -->
         <?php if (isset($build['field_type_2']['#items'][0]['value'])): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Avalanche Character #2: <?php print $type_2;?></span>
            <ul style="text-align:center;list-style:none;padding-left:0px;width:100%;max-width:7150px">
               <li style="display:inline-block;width:200px;vertical-align:top">
                  <div style="border-bottom:1px solid #ddd;text-transform:uppercase;font-size:17px;">
                  </div>
                  <div style="padding-top:10px;min-height:185px;border-bottom:1px solid #ddd;border-right:1px solid #ddd">
                     <a target="_blank" href="<?php print $url;?>/avalanche-problems#<?php print $build['field_type_2']['#items'][0]['value'];?>"><img style="height:185px;" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/api/<?php print $build['field_type_2']['#items'][0]['value'];?>.png" /></a>
                  </div>
               </li>
               <li style="display:inline-block;width:100%;max-width:470px;vertical-align:top">
                  <div style="border-bottom:1px solid #ddd;text-transform:uppercase;font-size:17px;">
                  </div>
                  <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:200px;font-size: 1.071em;">
                     <div class="problem-text" style="display:flex;justify-content:center;flex-direction:column;height: 175px;padding: 0px 10px;">
                        <?php $ac = $build['field_type_2']['#items'][0]['value']; 
                           $ct = array('','Storm Slab avalanches release naturally during snow storms and can be triggered for a few days after a storm. They often release at or below the trigger point. They exist throughout the terrain. Avoid them by waiting for the storm snow to stabilize.','Deep Slab avalanches are destructive and deadly events that can release months after the weak layer was buried. They are scarce compared to Storm or Wind Slab avalanches. Their cycles include fewer avalanches and occur over a larger region. You can triggered them from well down in the avalanche path, and after dozens of tracks have crossed the slope. Avoid the terrain identified in the forecast and give yourself a wide safety buffer to address the uncertainty.','Wind Slab avalanches release naturally during wind events and can be triggered for up to a week after a wind event. They form in lee and cross-loaded terrain features. Avoid them by sticking to wind sheltered or wind scoured areas.','Wet Slab avalanches occur when there is liquid water in the snowpack, and can release during the first few days of a warming period. Travel early in the day and avoid avalanche terrain when you see pinwheels, roller balls, loose wet avalanches, or during rain-on-snow events.','Persistent Slab avalanches can be triggered days to weeks after the last storm. They often propagate across and beyond terrain features that would otherwise confine Wind and Storm Slab avalanches. In some cases they can be triggered remotely, from low-angle terrain or adjacent slopes. Give yourself a wide safety buffer to address the uncertainty.','Loose Wet avalanches occur when water is running through the snowpack, and release at or below the trigger point. Avoid very steep slopes and terrain traps such as cliffs, gullies, or tree wells. Exit avalanche terrain when you see pinwheels, roller balls, a slushy surface, or during rain-on-snow events.','Loose Dry avalanches exist throughout the terrain, release at or below the trigger point, and can run in densely-treed areas. Avoid very steep slopes and terrain traps such as cliffs, gullies, or tree wells.','Generally safe avalanche conditions. Watch for unstable snow on isolated terrain features. Use normal caution when travelling in the backcountry.','Cornice Fall avalanches are caused by a release of overhanging, wind drifted snow. Cornices form on lee and cross-loaded ridges, sub-ridges, and sharp convexities. They are easiest to trigger during periods of rapid growth from wind drifting, rapid warming, or during rain-on-snow events. Cornices may break farther back onto flatter areas than expected.','Glide avalanches occur when water lubricates the interface between the snowpack and the ground. The entire snowpack releases. These avalanches are difficult to predict. Avoid the terrain identified in the forecast or below glide cracks.'
                           );
                           $ct_text = $ct[$ac];
                           print $ct_text; ?>
                     </div>
                  </div>
               </li>
            </ul>
            <div style="line-height:1.5em;padding:10px;font-size: 1.071em;"><?php print $build['field_description_2']['#items'][0]['value']; ?></div>
         </div>
         <?php endif; ?>
         <!--End problem 2 -->
         <!-- Prob 3 -->
         <?php if (isset($build['field_type_3']['#items'][0]['value'])): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Avalanche Character #3: <?php print $type_3;?></span>
            <ul style="text-align:center;list-style:none;padding-left:0px;width:100%;max-width:7150px">
               <li style="display:inline-block;width:200px;vertical-align:top">
                  <div style="border-bottom:1px solid #ddd;text-transform:uppercase;font-size:17px;">
                  </div>
                  <div style="padding-top:10px;min-height:185px;border-bottom:1px solid #ddd;border-right:1px solid #ddd">
                     <a target="_blank" href="<?php print $url;?>/avalanche-problems#<?php print $build['field_type_3']['#items'][0]['value'];?>"><img style="height:185px;" src="<?php print $url; ?>/sites/all/themes/responsive_sac/img/api/<?php print $build['field_type_3']['#items'][0]['value'];?>.png" /></a>
                  </div>
               </li>
               <li style="display:inline-block;width:100%;max-width:470px;vertical-align:top">
                  <div style="border-bottom:1px solid #ddd;text-transform:uppercase;font-size:17px;">
                  </div>
                  <div style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;min-height:200px;font-size: 1.071em;">
                     <div class="problem-text" style="display:flex;justify-content:center;flex-direction:column;height: 175px;padding: 0px 10px;">
                        <?php $ac = $build['field_type_3']['#items'][0]['value']; 
                           $ct = array('','Storm Slab avalanches release naturally during snow storms and can be triggered for a few days after a storm. They often release at or below the trigger point. They exist throughout the terrain. Avoid them by waiting for the storm snow to stabilize.','Deep Slab avalanches are destructive and deadly events that can release months after the weak layer was buried. They are scarce compared to Storm or Wind Slab avalanches. Their cycles include fewer avalanches and occur over a larger region. You can triggered them from well down in the avalanche path, and after dozens of tracks have crossed the slope. Avoid the terrain identified in the forecast and give yourself a wide safety buffer to address the uncertainty.','Wind Slab avalanches release naturally during wind events and can be triggered for up to a week after a wind event. They form in lee and cross-loaded terrain features. Avoid them by sticking to wind sheltered or wind scoured areas.','Wet Slab avalanches occur when there is liquid water in the snowpack, and can release during the first few days of a warming period. Travel early in the day and avoid avalanche terrain when you see pinwheels, roller balls, loose wet avalanches, or during rain-on-snow events.','Persistent Slab avalanches can be triggered days to weeks after the last storm. They often propagate across and beyond terrain features that would otherwise confine Wind and Storm Slab avalanches. In some cases they can be triggered remotely, from low-angle terrain or adjacent slopes. Give yourself a wide safety buffer to address the uncertainty.','Loose Wet avalanches occur when water is running through the snowpack, and release at or below the trigger point. Avoid very steep slopes and terrain traps such as cliffs, gullies, or tree wells. Exit avalanche terrain when you see pinwheels, roller balls, a slushy surface, or during rain-on-snow events.','Loose Dry avalanches exist throughout the terrain, release at or below the trigger point, and can run in densely-treed areas. Avoid very steep slopes and terrain traps such as cliffs, gullies, or tree wells.','Generally safe avalanche conditions. Watch for unstable snow on isolated terrain features. Use normal caution when travelling in the backcountry.','Cornice Fall avalanches are caused by a release of overhanging, wind drifted snow. Cornices form on lee and cross-loaded ridges, sub-ridges, and sharp convexities. They are easiest to trigger during periods of rapid growth from wind drifting, rapid warming, or during rain-on-snow events. Cornices may break farther back onto flatter areas than expected.','Glide avalanches occur when water lubricates the interface between the snowpack and the ground. The entire snowpack releases. These avalanches are difficult to predict. Avoid the terrain identified in the forecast or below glide cracks.'
                           );
                           $ct_text = $ct[$ac];
                           print $ct_text; ?>
                     </div>
                  </div>
               </li>
            </ul>
            <div style="line-height:1.5em;padding:10px;font-size: 1.071em;"><?php print $build['field_description_3']['#items'][0]['value']; ?></div>
         </div>
         <?php endif; ?>
         <!--End problem 3 -->
         <!--END AVALANCHE PROBLEMS -->
         <!--BEGIN AVALANCHE TEXT DISCUSSION-->
         <?php if ($build['field_text_discussion']['#items'][0]['value'] != ''): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">snowpack discussion</span>
            <div style="margin-bottom:5px;padding:5px;clear:both">
               <div style="padding:10px">
                  <?php print $build['field_text_discussion']['0']['#markup']; ?>
               </div>
            </div>
         </div>
         <?php endif; ?>
         <!--END AVALANCHE TEXT DISCUSSION-->
         <!--BEGIN RECENT OBS-->
         <?php if ($build['field_recent_activity']['#items'][0]['value'] != ''): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444"><?php if (!empty($obs_page)):?><a style="color:#eee;" href="<?php print $url.$obs_page; ?>"><?php endif;?>recent observations<?php if (!empty($obs_page)):?></a><?php endif;?></span>
            <div style="margin-bottom:5px;padding:5px;clear:both;min-height:165px;height:auto;">
               <div align="left" hspace="5" vspace="5" style="float:left;width:150px;padding-top:5px;">
                  <?php if (!empty($submit_snowpack_obs_page)):?>
                  <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                     <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$submit_snowpack_obs_page; ?>">Submit Snowpack Observations</a>
                  </div>
                  <?php endif;?>
                  <?php if (!empty($submit_avalanche_obs_page)):?>
                  <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                     <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$submit_avalanche_obs_page; ?>">Submit Avalanche Observations</a>
                  </div>
                  <?php endif;?>
               </div>
               <div style="padding-left:150px">
                  <?php print $build['field_recent_activity']['0']['#markup']; ?>
               </div>
            </div>
         </div>
         <?php endif; ?>
         <!--END RECENT OBS-->
         <!--BEGIN WEATHER OBS - CURRENT CONDITIONS-->
         <?php if (isset($build['field_temp8700']['#items']['0']['value'])): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">CURRENT CONDITIONS</span><span style="color: #444;margin-bottom: 5px;">&nbsp; <?php print $current_wx_desc;?></span>
            <div style="margin-bottom:5px;padding:5px;clear:both;min-height:165px;height:auto;">
               <div align="left" hspace="5" vspace="5" style="float:left;width:150px;padding-top:5px;">
                  <?php if (!empty($wx_map_page)):?>	
                  <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                     <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$wx_map_page; ?>">Weather Station Map</a>
                  </div>
                  <?php endif;?>
                  <?php if (!empty($wx_table_page)):?>
                  <div style="background-color: #4cb7f7;border-radius: 5px;font-weight: bold;margin: 5px;padding: 2px;text-align: center;max-width: 130px;">
                     <a style="color: #ffffff;white-space: normal !important;" href="<?php print $url.$wx_table_page; ?>">Weather Station Table</a>
                  </div>
                  <?php endif;?>
               </div>
               <table style="padding:5px;margin:0px;width:80%;border:none">
                  <tbody>
                     <tr>
                        <td style= "background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           0600 temperature:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_temp8700']['#items']['0']['value']);?> deg. F.
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Max. temperature in the last 24 hours:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24maxtemp']['#items']['0']['value']);?> deg. F.
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Average wind direction during the last 24 hours:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24winddir']['#items']['0']['value']);?>
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Average wind speed during the last 24 hours:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24windspeed']['#items']['0']['value']);?> mph
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Maximum wind gust in the last 24 hours:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24maxgust']['#items']['0']['value']);?> mph
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           New snowfall in the last 24 hours:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_hr24snowfall']['#items']['0']['value']);?> inches
                        </td>
                     </tr>
                     <tr>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           Total snow depth:
                        </td>
                        <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                           <?php print render($build['field_totalsnowdepth']['#items']['0']['value']);?> inches
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <?php endif; ?>
         <!-- BEGIN MOUNTAIN WEATHER-->    
         <?php if ($build['field_mountain_weather']['#items'][0]['value'] != ''): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">weather</span>
            <div style="margin-bottom:5px;padding:5px;clear:both">
               <div style="padding:10px">
                  <?php print $build['field_mountain_weather']['0']['#markup']; ?>
               </div>
            </div>
         </div>
         <?php endif; ?>
         <!-- END MOUNTAIN WEATHER--> 
         <!--TWO DAY WEATHER FORECAST-->
         <?php if (isset($build['field_today7to8weather']['#items']['0']['value'])): ?>
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Two-Day Mountain Weather Forecast</span><span style="color: #444;margin-bottom: 5px;">&nbsp; Produced in partnership with the <a href="<?php print $nws_url;?>" target="_blank"><?php print $nws_name;?></a>
            </span>
            <table style="padding:5px;margin:0px;width:100%">
               <tbody>
                  <tr>
                     <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px">
                        <table cellpadding="5" border="0" cellspacing="5" width="100%" style="padding:5px;margin:0px">
                           <tbody>
                              <tr>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal" colspan="4"><?php print $wx_low;?></th>
                              </tr>
                              <tr>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    &nbsp;
                                 </th>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    <?php 
                                       if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                                       print "Today";
                                       }
                                       else { 
                                       echo date("l", $build['#node']->created);
                                       } 
                                       ?>
                                 </th>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    <?php 
                                       if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                                       print "Tonight";
                                       }
                                       else { 
                                       echo date("l", $build['#node']->created)." Night";
                                       }
                                       ?>
                                 </th>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    <?php 
                                       $tomorrow  = strtotime('+1 day', $build['#node']->created);
                                       echo date("l", $tomorrow);
                                          ?>
                                 </th>
                              </tr>
                              <tr>
                                 <td width="15%" style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Weather:
                                 </td>
                                 <td width="28%" style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today7to8weather']['#items']['0']['value']) ?>
                                 </td>
                                 <td width="28%" style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight7to8weather']['#items']['0']['value']) ?>
                                 </td>
                                 <td width="28%" style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow7to8weather']['#items']['0']['value']) ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Temperatures:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today7to8temp']['#items']['0']['value']) ?> deg. F.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight7to8temp']['#items']['0']['value']) ?> deg. F.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow7to8temp']['#items']['0']['value']) ?> deg. F.
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Wind direction:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today7to8winddirection']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight7to8winddirection']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow7to8winddirection']['#items']['0']['value']) ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Wind speed:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today7to8windspeed']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight7to8windspeed']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow7to8windspeed']['#items']['0']['value']) ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Expected snowfall:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today7to8snow']['#items']['0']['value']) ?> in.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight7to8snow']['#items']['0']['value']) ?> in.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow7to8snow']['#items']['0']['value']) ?> in.
                                 </td>
                              </tr>
                              <tr>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal" colspan="4"><?php print $wx_high;?></th>
                              </tr>
                              <tr>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    &nbsp;
                                 </th>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    <?php 
                                       if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                                       print "Today";
                                       }
                                       else { 
                                       echo date("l", $build['#node']->created);
                                       } 
                                       ?>
                                 </th>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    <?php 
                                       if (date('Y-m-d') == date('Y-m-d', $build['#node']->created)) {
                                       print "Tonight";
                                       }
                                       else { 
                                       echo date("l", $build['#node']->created)." Night";
                                       }
                                       ?>
                                 </th>
                                 <th style="color:#3b3b3b;background-color:#fff;text-align:center;font-weight:normal">
                                    <?php 
                                       $tomorrow  = strtotime('+1 day', $build['#node']->created);
                                       echo date("l", $tomorrow);
                                          ?>
                                 </th>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Weather:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today8to9weather']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight8to9weather']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow8to9weather']['#items']['0']['value']) ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Temperatures:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today8to9temp']['#items']['0']['value']) ?> deg. F.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight8to9temp']['#items']['0']['value']) ?> deg. F.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow8to9temp']['#items']['0']['value']) ?> deg. F.
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Wind direction:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today8to9winddirection']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight8to9winddirection']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow8to9winddirection']['#items']['0']['value']) ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Wind speed:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today8to9windspeed']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight8to9windspeed']['#items']['0']['value']) ?>
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow8to9windspeed']['#items']['0']['value']) ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    Expected snowfall:
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_today8to9snow']['#items']['0']['value']) ?> in.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tonight8to9snow']['#items']['0']['value']) ?> in.
                                 </td>
                                 <td style="background-color:#fff;vertical-align:top;line-height:1.5em;font-size:13px;border:2px solid rgb(126,41,11);text-align:center">
                                    <?php print render($build['field_tomorrow8to9snow']['#items']['0']['value']) ?> in.
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <?php endif; ?>
         <!--END TWO DAY WEATHER FORECAST-->
         <!--BEGIN DISCLAIMER-->
         <div style="background-color:transparent;color:#444;margin-bottom:5px;border:1px solid #ddd">
            <span style="color:#eee;padding:7px;margin:0px;text-transform:uppercase;font-weight:bold;font-size:17px;background-color:#444">Disclaimer</span>
            <div style="margin-bottom:5px;padding:5px;clear:both">
               <div style="padding:10px">
                  <?php print $build['field_disclaimer']['#items'][0]['value']; ?>
               </div>
            </div>
         </div>
      </div>
      <!-- end disclaimer-->
   </div>
</div>
<!-- 
   <pre>
   <?php print_r($build['field_forecast_region']['#items']);?>
   </pre>
   -->
